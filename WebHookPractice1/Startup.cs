﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebHookPractice1.Startup))]
namespace WebHookPractice1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
